import SwiftUI

struct PageView :View {

    @State var currentPage = 0
    static var viewControllers = [
    UIHostingController(rootView: BoxFaceCreateScene()),
    UIHostingController(rootView: BoxItemCreate01Scene()),
    UIHostingController(rootView: BoxItemCreate02Scene()),
    UIHostingController(rootView: BoxItemCreate03Scene()),
    UIHostingController(rootView: DecideScene())]
    
    var body: some View {
        ZStack(alignment: .bottom) {
            PageViewController(controllers: PageView.viewControllers
                ,
                               currentPage: $currentPage)
            PageControl(numberOfPages: PageView.viewControllers.count,
                        currentPage: $currentPage)
        }
    }
}

