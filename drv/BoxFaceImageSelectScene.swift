//
//  BoxFaceImage_Scene.swift
//  drv
//
//  Created by け on 2020/08/23.
//  Copyright © 2020 ke. All rights reserved.
//

import SwiftUI

struct BoxFaceImageSelectScene: View {
    //環境変数の定義
    @EnvironmentObject var datastore: DataStore
    
    var body: some View {
            ZStack {
                //背景色を設定
                    Color(self.datastore.boxColor)
                    .edgesIgnoringSafeArea(.all)
                    VStack {
                       
                        Text("イメージを選択してください")
                            .font(.body)
                            .foregroundColor(Color.white)
                            .padding()
                        //Image1の選択欄
                        Button(action: {
                            self.datastore.boxFaceImage = "BoxImage1"
                        }) {
                            Image("BoxImage1")
                            .resizable()
                            .renderingMode(.original)
                            .frame(width: 310, height: 200)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                        }
                       
                            //Image2の選択欄
                            Button(action: {
                                self.datastore.boxFaceImage = "BoxImage2"
                            }) {
                                Image("BoxImage2")
                                .resizable()
                                .renderingMode(.original)
                                .frame(width: 310, height: 200)
                                .shadow(color: .black, radius: 1, x: 2, y: 2)
                            }
                            //Image3の選択欄
                           Button(action: {
                            self.datastore.boxFaceImage = "BoxImage3"
                           }) {
                                Image("BoxImage3")
                                .resizable()
                                .renderingMode(.original)
                                .frame(width: 310, height: 200)
                                .shadow(color: .black, radius: 1, x: 2, y: 2)
                            }
                           
                }
            }
        }
    }

