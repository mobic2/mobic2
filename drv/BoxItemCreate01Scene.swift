//
//  InputEiga.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftUI

struct BoxItemCreate01Scene: View {
    @State public var itemTitle = ""
    @State public var itemComment = ""
    // 画面遷移フラグ
    @State var isActiveSubView = false
    @State var image: UIImage?
    @State public var inputSe: Int = 0
    let data = Data()
    
    @EnvironmentObject var datastore: DataStore
    
    public var realm = try! Realm()
    var eiga: Eiga!

    var body: some View {
        ZStack {
            // 背景色をボックスカラーに設定
            Color(self.datastore.boxColor)
                // 背景色の範囲を全画面に拡大
                .edgesIgnoringSafeArea(.all)
           
            VStack(spacing: 280) {
                Spacer()
                HStack {
                    // アイテムタイトル(映画タイトル)の入力欄
                    TextField("", text: $itemTitle)
                        .frame(height: 42)
                        // .padding(.vertical, 7.0)
                        // 入力される文字の色は白に設定
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.center)
                        // .onAppear → ビュー遷移時の処理
                        .onAppear {
                            self.itemTitle = "default text"
                        }
                }
                .padding(.top)
                // 入力欄の背景色
                .background(Color("sukedark"))
                //Spacer()
                //Spacer()
                // アイテムイメージ画面の呼び出し
                ImagePickAndDisplaySwiftUIView2(inputSe: self.$inputSe)
                .padding(.init(top: -260,
                leading: 0,
                bottom: -180,
                trailing: 0))
                //Spacer()
                // アイテム(映画)へのコメント入力欄の設定
                HStack {
                    Spacer()
                                    
                    TextField("テスト", text: $itemComment)
                        
                        // コメント数は5行までに制限
                        .lineLimit(5)
                        // 文字色
                        .foregroundColor(Color.white)
                        .frame(width: 350, height: 160)
                        // コメント欄の背景色
                        .background(Color("sukedark"))
                        .cornerRadius(12)
                    Spacer()
                }
                .padding(.init(top: -290,
                leading: 0,
                bottom: 140,
                trailing: 0))
                                    
                //Spacer()
            }
        }
    }
}

struct BoxItemCreate01Scene_Previews: PreviewProvider {
    static var previews: some View {
        BoxItemCreate01Scene().environmentObject(DataStore())
    }
}
