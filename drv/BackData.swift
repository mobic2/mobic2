//
//  EnvironmentData.swift
//  drv
//
//  Created by け on 2020/08/19.
//  Copyright © 2020 ke. All rights reserved.
//

import Foundation
import SwiftUI

class DataStore: ObservableObject {
    @Published var boxColor = "teacolor"  // ID
    @Published var boxTitle = ""
    @Published var boxFaceImage = ""
    @Published var boxNum = ""
    @Published var boxItemTitle: [String] = ["","",""]
    @Published var boxItemImage:
        [String] = ["","",""]
    @Published var boxItemComment:[String] = ["","",""]
    @Published var inputNumber = 0


}

struct BackData_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
