//
//  ContentView.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//
import Firebase
import RealmSwift
import SwiftUI


struct ContentView: View {
    // var boxArray = try! Realm().objects(Box.self).sorted(byKeyPath: "date", ascending: true)
    
    // var eigaArray = try! Realm().objects(Eiga.self).sorted(byKeyPath:gb "eiga_id", ascending: true
 
    
    init() {
        UINavigationBar.appearance().tintColor = .systemGray2
        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 0.10, green: 0.10, blue: 0.10, alpha: 1.0)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 25)]
        UITabBar.appearance().barTintColor = UIColor(displayP3Red: 0.10, green: 0.10, blue: 0.10, alpha: 1.0)
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            NavigationView {
                TabView {
                    HomeScene()
                        .tabItem {
                            Image(systemName: "house.fill")
                        }
                    BoxDetailScene01()
                        .tabItem {
                            Image(systemName: "house.fill")
                        }
                    PageView()
                        .tabItem {
                            Image(systemName: "house.fill")
                        }
                }
                
                .navigationBarTitle("", displayMode: .inline)
            }
             Image("mobic_logo")
                           .resizable()
                           .mask(Color(red: 0.0, green: 0.0, blue: 0.0, opacity: 0.5))
                           .scaledToFit()
                           .frame(width: nil, height: 40, alignment: .center)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
